import unittest
import numpy as np

from chelsea_uvilux_driver import parse_uvilux_bytes
import chelsea_uvilux_msgs.msg


class TestParsing(unittest.TestCase):
    # Example of valid string from Craig's documentation
    docs_example = "+000.000,400,20\r\n".encode("utf-8")

    # Example of valid string from my playing with the instrument
    gtd_example = "+000.000,400,20\r\n".encode("utf-8")

    def test_good_input(self):
        for input in [self.docs_example, self.gtd_example]:
            msg = parse_uvilux_bytes(input)
            self.assertIsInstance(msg, chelsea_uvilux_msgs.msg.UviluxFluorescence)

    def test_invalid_bytes(self):
        """
        0x9c, 0xa5 Are both examples of invalid unicode bytes

        Insert an undecodeable byte at a random position of the input, confirm
        that the parser catches the error.
        """
        for _ in np.arange(5):
            bad_bytearray = bytearray(self.docs_example)
            idx = np.random.randint(len(bad_bytearray))
            bad_bytearray[idx] = 0x9C
            bad_input = bytes(bad_bytearray)
            msg = parse_uvilux_bytes(bad_input)
            self.assertIsNone(msg)

    def test_too_few_tokens(self):
        # simply removed single field from docs_example
        input_data = "+000.000,400\r\n".encode("utf-8")
        msg = parse_uvilux_bytes(input_data)
        self.assertIsNone(msg)

    def test_too_many_tokens(self):
        # repeated field in docs_example
        input_data = "+000.000,400,20,30\r\n".encode("utf-8")
        msg = parse_uvilux_bytes(input_data)
        self.assertIsNone(msg)

    def test_wrong_types(self):
        # Turned an int into a float in docs_example

        input_data = "+000.000,400,20.12\r\n".encode("utf-8")
        msg = parse_uvilux_bytes(input_data)
        self.assertIsNone(msg)


if __name__ == "__main__":
    unittest.main()
