from typing import Any, Optional
import rospy

import chelsea_uvilux_msgs.msg


def is_integer(n: Any) -> Optional[int]:
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()


def parse_uvilux_bytes(
    data: bytes,
) -> Optional[chelsea_uvilux_msgs.msg.UviluxFluorescence]:
    """
    String received from the Uvilux s is CSV (ASCII, utf-8 encoded into raw bytes)

    At start-up the instrument sends a brief header:

    (which may be hard to parse)

    The standard output is:

    # Fluorescence, EHT voltage, data quality
    +000.000,400,20

    Data quality is a 2-digit hex bit field:

    0x01   ADC clipping
    0x02   EHT changed
    0x04   Low signal
    0x08   High signal
    0x10   Negative fluorescence
    0x20   Sampling error

    The unit can also be configured to (optionally) display the onboard
    tempearture and internal 15V voltage (Vin)

    # Fluorescence, Vin, Temperature, EHT, Data quality
    +000.000,+014.88Vin,+022.9'C,400,20

    If parsing fails, returns None.
    Does not raise exceptions.

    If parsing succeeds, returns UviluxFluorescence message.
    Header will be blank.

    Example data string from sensor:
    "\r\n"

    """
    try:
        # We want decode errors to raise a UnicodeDecodeError, rather than trying
        # to recover and potentially introducing corrupt data.
        data_str = data.decode("utf-8", errors="strict")
    except Exception as ex:
        # I think UnicodeDecodeError is the only exception decode raises, but I'm not
        # positive about that, so go ahead and catch anything.
        rospy.logerr(
            "{} \n Unable to decode bytes into utf-8! {}".format(ex, list(data))
        )
        return None

    data_str = data_str.strip()

    # Data strings always start with a {+,-} all other are headers
    # Send those to roslog as information
    if data_str[0] not in "+-" and len(data_str) > 0:
        rospy.loginfo("Uvilux header: {}".format(data_str))
        return None

    tokens = data_str.split(",")
    num_fields = 3  # magic number -- expected number of fields in a message.
    if len(tokens) != num_fields:
        rospy.logerr(
            "Incorrect number of fields in data. expected {}, got {}: input = {}. ".format(
                num_fields, len(tokens), data_str
            )
        )
        return None

    fluor, eht_volts, qual_flags = tokens
    if not is_integer(qual_flags) or int(qual_flags) < 0 or int(qual_flags) > 255:
        rospy.logerr("Unexpected value for quality flags value {}. ".format(qual_flags))
        return None

    msg = chelsea_uvilux_msgs.msg.UviluxFluorescence()
    try:
        msg.fluorescence = float(fluor)
        msg.eht_voltage = float(eht_volts)
        msg.quality_flag = int(qual_flags, 16)
    except Exception as ex:
        rospy.logerr("{} \n Unable to decode voltage values: {}".format(ex, tokens))
        return None

    return msg
