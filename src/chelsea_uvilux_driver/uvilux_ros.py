#! /usr/bin/env python3
"""
Driver for Chelsea Uvilux sensor

https://chelsea.co.uk/products/uvilux/
"""

import rospy

import apl_msgs.msg
import chelsea_uvilux_msgs.msg
import serial


from .uvilux_parser import parse_uvilux_bytes


class ChelseaUviluxDriver:
    def __init__(self) -> None:
        rospy.logdebug("ChelseaUviluxDriver.__init__")
        self.setup_parameters()

        self.raw_pub = rospy.Publisher("~raw", apl_msgs.msg.RawData, queue_size=1)
        self.gtd_pub = rospy.Publisher(
            "~fluorescence", chelsea_uvilux_msgs.msg.UviluxFluorescence, queue_size=1
        )

        # For Craig's sensors, baudrate is fixed. I asked if he wanted it configurable,
        # and he didn't say yes.
        baudrate = 9600
        serial_timeout = 2.0  # seconds
        self.serial = serial.Serial(
            port=self.port, baudrate=baudrate, timeout=serial_timeout
        )

        self.termination = bytes([0x0D, 0x0A])

    def setup_parameters(self) -> None:
        rospy.logdebug("ChelseaUviluxDriver.setup_parameters")
        self.port = rospy.get_param("~port")
        self.frame_id = rospy.get_param("~frame_id")

    def publish_raw(self, data: bytes, stamp: rospy.Time, frame_id: str) -> None:
        """Publish received data to the raw topic"""
        raw_msg = apl_msgs.msg.RawData()
        raw_msg.header.stamp = stamp
        raw_msg.header.frame_id = frame_id
        raw_msg.direction = raw_msg.DATA_IN
        raw_msg.data = data
        self.raw_pub.publish(raw_msg)

    def run(self) -> None:
        rospy.logdebug("ChelseaUviluxDriver.run")
        # First, clear stale data
        data = self.serial.read_all()
        # I'm surprised that mypy  made me check for `is not None` here; I would have
        # expected `read_all` to have the same return type as `read_until`
        if data is not None and len(data) > 0:
            rospy.loginfo("Cleared {} bytes of data".format(len(data)))

        while not rospy.is_shutdown():
            try:
                data = self.serial.read_until(self.termination)
                data_stamp = rospy.Time.now()
                if len(data) > 0:
                    self.publish_raw(data, data_stamp, self.frame_id)
                    msg = parse_uvilux_bytes(data)

                    if msg is not None:
                        msg.header.stamp = data_stamp
                        msg.header.frame_id = self.frame_id
                        self.gtd_pub.publish(msg)

            except Exception as ex:
                rospy.logerr("Unhandled Exception! {}".format(ex))


def main() -> None:
    rospy.init_node("chelsea_uvilux_driver")
    gtd = ChelseaUviluxDriver()
    gtd.run()
